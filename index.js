import { ApplePay } from './index.ios';
import { GooglePay } from './index.android';
import { Platform } from 'react-native';

class RnPayments {
  ENVIRONMENT_TEST = Platform.OS !== 'ios' ? GooglePay.ENVIRONMENT_TEST : 0;
  ENVIRONMENT_PRODUCTION = Platform.OS !== 'ios' ? GooglePay.ENVIRONMENT_PRODUCTION : 0;
  SUCCESS = Platform.OS === 'ios' ? ApplePay.SUCCESS : 0;
  FAILURE = Platform.OS === 'ios' ? ApplePay.FAILURE : 0;
  async isReadyToPay(allowedCardNetworks, allowedCardAuthMethods) { return new Promise((resolve, reject) => {
    if (Platform.OS === 'ios') {
      resolve(ApplePay.canMakePayments);
    } else {
      if (allowedCardNetworks && allowedCardAuthMethods) {
        GooglePay.isReadyToPay(allowedCardNetworks, allowedCardAuthMethods).then(ready => {
          resolve(ready);
        }).catch(err => {
          reject(err);
        })
      } else {
        reject('GooglePay isReadyToPay method should be with allowedCardNetworks and allowedCardAuthMethods');
      }
    }
  })}
  async requestPayment (requestData) {
    return new Promise((resolve, reject) => {
      if (Platform.OS === 'ios') {
        ApplePay.requestPayment(requestData)
          .then((paymentData) => {
            resolve(paymentData)
          }).catch(error => reject(error));
      } else {
        GooglePay.requestPayment(requestData)
          .then(token => {
            resolve(token);
          }).catch(error => reject(error));
      }
    })
  }
  async complete (status) {
    return new Promise((resolve, reject) => {
      if (Platform.OS === 'ios') {
        ApplePay.complete(status).then(() => {
          resolve();
        }).catch(err => {
          reject(err);
        })
      } else {
        reject('GooglePay has no "complete" function');
      }
    })
  }

  setEnvironment (environment) {
    if (Platform.OS !== 'ios') {
      GooglePay.setEnvironment(environment);
    }
  }
}

const Payments = new RnPayments();

export { Payments };