import {
  AllowedCardNetworkType,
  AllowedCardAuthMethodsType,
  RequestDataType as GpayRequestDataType,
} from './index.android.d';
import {
  APayRequestDataType,
  APayAllowedCardNetworkType,
  APayPaymentSummaryItemType,
} from './index.ios.d';
declare class Payments {
  static isReadyToPay: (
    allowedCardNetworks?: AllowedCardNetworkType[],
    allowedCardAuthMethods?: AllowedCardAuthMethodsType[],
  ) => Promise<boolean | string>
  static requestPayment: (requestData: GpayRequestDataType | APayRequestDataType) => Promise<string>
  static setEnvironment: (environment: number) => void
  static ENVIRONMENT_TEST: number
  static ENVIRONMENT_PRODUCTION: number
  static SUCCESS: number
  static FAILURE: number
  static complete: (status: number) => Promise<string | void>
}

export {
  Payments,
  AllowedCardNetworkType as GpayAllowedCardNetworkType,
  AllowedCardAuthMethodsType as GpayAllowedCardAuthMethodsType,
  APayRequestDataType,
  APayAllowedCardNetworkType,
  APayPaymentSummaryItemType,
  GpayRequestDataType,
};